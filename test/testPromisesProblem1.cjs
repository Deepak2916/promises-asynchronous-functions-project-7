const path = require('path')
const problem1 = require('../promises-problem1.cjs')

let randomNumberOfFiles = Math.floor(Math.random() * 10)
if (randomNumberOfFiles < 1) {
    randomNumberOfFiles = 5
}

const absolutePathOfRandomDirectory = path.join(__dirname,'randomDirectory')

problem1(absolutePathOfRandomDirectory, randomNumberOfFiles)
