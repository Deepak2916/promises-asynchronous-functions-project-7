
// Using callbacks and the fs module's asynchronous functions, do the following:
//     1. Read the given file lipsum.txt
//     2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
//     3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
//     4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
//     5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.

const fs = require('fs')
const path = require('path')



function problem2() {

    function readFiles(filePath) {
        filePath = path.join(__dirname, filePath)

        return new Promise((res, rej) => {

            fs.readFile(filePath, 'utf8', function (error, data) {
                if (error) {
                    rej(error)
                } else {
                    res(data)

                }

            })
        })
    }
    function createFileOrAppendData(filePath, data) {
        filePath = path.join(__dirname, filePath)
        return new Promise((res, rej) => {
            fs.appendFile(filePath, data, function (error) {
                if (error) {
                    rej(error)
                } else {
                    const fileName = filePath.split('/').pop()

                    if (fileName === 'filenames.txt') {

                        res(data.split('\n')[0])
                    }
                    else {
                        console.log(`${fileName} created`)
                        res(fileName)
                    }
                }
            })
        })
    }
    function deleteAllFiles(arrayOfFiles) {

        for (let file of arrayOfFiles) {
            let filePath = path.join(__dirname, file)
            fs.unlink(filePath, (error) => {
                if (error) {
                    console.log(error);
                } else {
                    console.log(`${file} deleted`);
                }
            })
        }
    }
    readFiles('./lipsum.txt')
        .then((data) => {
            let newData = data.toUpperCase()

            return createFileOrAppendData('./fileOne.txt', newData)
        })
        .then((fileName) => {

            return createFileOrAppendData('./filenames.txt', fileName + '\n')
        })
        .then((fileName) => {

            return readFiles(`./${fileName}`)
        })
        .then(data => {
            let newData = data.toLowerCase().split('.').join('\n')

            return createFileOrAppendData('./fileTwo.txt', newData)
        })
        .then(fileName => {
            return createFileOrAppendData('./filenames.txt', fileName + '\n')
        })
        .then(fileName => {
            return readFiles(`./${fileName}`)
        })
        .then(data => {
            let newData = data.split('\n').sort().join('\n').trim()

            return createFileOrAppendData('./fileThree.txt', newData)
        })
        .then(fileName => {
            return createFileOrAppendData('./filenames.txt', fileName)
        })
        .then(() => {
            return readFiles('./filenames.txt')
        })
        .then((data) => {

            const arrayOfFiles = data.split('\n')
            deleteAllFiles(arrayOfFiles)
        })
        .catch((error) => {
            console.log(error)
        })

    // function readFiles(filePath, newfile) {

    // }

    // function createNewFile(filePath, data) {
    //     console.log(`${filePath.slice(2)} created`)

    //         else {

    //             if (filePath === './newfile1.txt') {

    //                 readFiles('./newfile1.txt', 'newfile2.txt')
    //             }
    //             else if (filePath === './newfile2.txt') {

    //                 readFiles('./newfile2.txt', 'newfile3.txt')
    //             }
    //             else if (filePath === './newfile3.txt') {
    //                 deleteFiles()
    //             }
    //         }
    //     })
    // }
    // function appendFiles(fileName) {
    //     fs.appendFile('./filenames.txt', fileName, (error) => {
    //         if (error) {
    //             console.log(error)
    //         }
    //     })
    // }

    // function deleteFiles() {
    //     fs.readFile('./filenames.txt', 'utf8', function (error, data) {
    //         data = data.split('\n')
    //         data.map(file => {
    //             fs.unlink(`${file}`, function (error) {
    //                 if (error) {
    //                     console.log(error)
    //                 } else {
    //                     console.log(`${file} deleted`)
    //                 }
    //                 return true

    //             })
    //         })

    //     })

    // }

}

module.exports = problem2