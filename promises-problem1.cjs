const fs = require('fs')



function problem1(directoryPath, numberOfFiles) {

    function createDirectory(numberOfFiles) {

        return new Promise((res, rej) => {

            fs.mkdir(directoryPath, function (error) {
                if (error) {
                    rej(error)
                } else {
                    console.log('randomDirectory created')
                    res(numberOfFiles)
                }
            })
        })
    }

    function randomJSONFiles(numberOfFiles) {

        return new Promise((res, rej) => {
            let totalFiles = []
            for (let index = 1; index <= numberOfFiles; index++) {

                fs.writeFile(`${directoryPath}/randomFile${index}.json`, '{}', (err) => {
                    if (err) {
                        rej(err)
                    } else {
                        console.log(`randomFile${index}.json created`)
                        totalFiles.push(`randomFile${index}.json`)
                        if (index === numberOfFiles) {
                            res(totalFiles)
                        }
                    }
                })
            }
        })

    }

    function deleteFilesSimultaneously(totalFiles) {

        for (let file of totalFiles) {

            fs.unlink(`${directoryPath}/${file}`, function (error) {
                if (error) {

                    console.log(error);
                } else {
                    console.log(`${file} deleted`);
                }
            })
        }


    }

    createDirectory(numberOfFiles)
        .then((numberOfFiles) => {
            return randomJSONFiles(numberOfFiles)
        })
        .then((res) => {

            deleteFilesSimultaneously(res)
        })
}


module.exports = problem1